import { Component } from '@angular/core';

import { appService } from "./_services/app.service";
import { calcService } from "./_services/calc.service";

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})

export class AppComponent {

	imageRotateName:string = '_default.jpg';
	currentPage:string = 'params';
	isCompleteParams:boolean = true;

	//currentPage:string = 'term';
	//isCompleteParams:boolean = true;	

	constructor(
		private app: appService,
		private calc: calcService
	){ }

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	clickMenu(s:string)
	{
		this.currentPage = s;
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	clickPredefinedModel(n:number):void
	{
		//console.log('clickPredefinedModel', n);
		this.calc.setPredefinedModel(n);
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	private updateRotateImageName()
	{
		var self = this;
		
		//rotate_house
		//100_okna__widok_1.jpg
	/*
	powierzchnia
	szyby
	zacienienie
	*/	
		var pw = '100';
		var zc = '';
		var kt = 1;
		
		if(this.calc.setData.okna_powierzchnia == 2)
			pw = '70';
		else if(this.calc.setData.okna_powierzchnia == 3)
			pw = '40';

		if(this.calc.setData.okna_zacienienie == 1)
			zc = '_rolety';
		else if(this.calc.setData.okna_zacienienie == 2)
			zc = '_okap';

		var d:any = {0:6, 45:5, 90:4, 135:3, 180:2, 225:1, 270:8, 315:7};
		
		if(d[this.calc.setData.angle])
			kt = d[this.calc.setData.angle];

		var src = pw + '_okna'+zc+'__widok_'+kt+'.jpg';
		
		this.imageRotateName = src;
	}
	
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -	

	ngOnInit(): void
	{
		//this.app.loader('askdjhakdjhadkda skdjh kjfhkj');

		this.calc.observer$.subscribe((ts:any = '', add:any = null) => {		

			if(ts == 'setPredefinedModel')
				this.updateRotateImageName();	
			else if(ts == 'changeAngleFromRotator' || ts == 'changeFromWiz')
				this.updateRotateImageName();
			else if(ts == 'changeFromAku')
				this.updateRotateImageName();
			else if(ts == 'nextFormParams')
			{
				this.currentPage = '';
				this.isCompleteParams = true;
			}
			else if(ts == 'changeFromParamsAngle' || ts == 'changeFromParamsWindows')
				this.updateRotateImageName();
			else
				console.log('app.component.observer', ts);
		});

		this.updateRotateImageName();


		
	}
}