import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppStartComponent } from './app-start/app-start.component';


import { HttpClientModule } from '@angular/common/http';
import { PageParamsComponent } from './components/page-params/page-params.component';
import { PageAirComponent } from './components/page-air/page-air.component';
import { PageTermComponent } from './components/page-term/page-term.component';
import { PageAkuComponent } from './components/page-aku/page-aku.component';
import { RotatorComponent } from './components/rotator/rotator.component';
import { PageWizComponent } from './components/page-wiz/page-wiz.component';
import { GgMapComponent } from './components/gg-map/gg-map.component';
import { MsgComponent } from './components/msg/msg.component';


@NgModule({
  declarations: [
    AppComponent,
    AppStartComponent,
    PageParamsComponent,
    PageAirComponent,
    PageTermComponent,
    PageAkuComponent,
    RotatorComponent,
    PageWizComponent,
    GgMapComponent,
    MsgComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
