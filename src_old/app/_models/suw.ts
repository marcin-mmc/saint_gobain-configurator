declare const $:any;

export class suw
{
	sName:any;
	idDv:any;
	points:any;
	options:any;

	callableFunction:any = null;

	suwHeight = 6;
	pointSize = 14;

	isDownMouse:boolean = false;
	isDownMouseWait:boolean = false;
	mouseMovePosition:number = 0;

	currentValue:any;

	constructor(idName:any, points:object, opt:object = {}, fn:any=null, _name:any=null)
	{
		var self = this;
		this.sName = _name == undefined? idName : _name ;
		this.idDv = $('#'+idName);
		
		this.points = points;
		this.options = opt;

		this.generateHTML();

		if(fn && typeof fn == 'function')
			this.callableFunction = fn;

		$('.suwLayer', this.idDv).click(function(e:any){
			if(!self.isDownMouseWait)
				self.movePointOnClick(e.offsetX);
		});

		$('.suwLayer > div', this.idDv).on('mousedown', function(e:any){
			e = e || window.event;
			e.preventDefault();
			self.isDownMouse = true;
			self.isDownMouseWait = true;
			//$('.suwLayer > div', self.idDv).css('cursor', 'move');
		});

		$('.suwOutline', this.idDv).on('mouseup mouseleave', function(){

			if(self.isDownMouse)
				self.movePointOnClick(self.mouseMovePosition);

			self.isDownMouse = false;
			setTimeout(function(){self.isDownMouseWait = false;}, 10);
		});

		$('.suwOutline', this.idDv).on('mousemove', function(e:any){

			if(self.isDownMouse)
			{
				let _ps = e.pageX - $('.suwLayer', self.idDv).offset().left;
				self.mouseMovePosition = _ps;
				$('.suwLayer > div', self.idDv).css({left:_ps - self.pointSize/2});
			}
		});

		$('.suwOutline .tbl span', this.idDv).click(function(e:any){
			let n = $(e.currentTarget).attr('data-suw-index');
			self._setPointOnKey(parseInt(n));
		});
			

		if(this.options.val != undefined)
			this.setVal(this.options.val);

		//idDv.html('asdasdas');		
	}

	private tryCallF(n:number)
	{
		var val = this.points[n].val || this.points[n].label;
		
		this.currentValue = val;
		
		if(this.callableFunction && this.points[n])
			this.callableFunction.apply(null, [val, this.sName]);
	}

	private movePointOnClick(ps:number):void
	{
		var sw = $('.suwLayer', this.idDv).width();

		var dz = sw/this.points.length;

		let j = 0;
		let z = -1;
		for(let cx=0; cx<sw; cx+=dz)
		{
			if(ps >= cx && ps <= dz*(j+1))
				z = j;

			j++;
		}
		if(z > -1)
		{
			//console.log(z);
			this.tryCallF(z);
			$('.suwLayer > div', this.idDv).css({left : ((z*dz)+dz/2-this.pointSize/2)+'px'});
		}
	}

	setVal(val:any):void
	{
		for(let i=0; i<this.points.length; i++)
		{
			let _v = this.points[i].val != undefined? this.points[i].val : this.points[i].label ;
			if(_v  == val)
			{
				this.currentValue = val;
				this._setPointOnKey(i);
			}
		}
	}

	private _setPointOnKey(n:number)
	{
		if(n > -1)
		{
			let sw = $('.suwLayer', this.idDv).width();
			let dz = sw/this.points.length;
			$('.suwLayer > div', this.idDv).css({left : ((n*dz)+dz/2-this.pointSize/2)+'px'});
			
		}
	}

	private generateHTML():void
	{
		var body = '';

		body += '<div class="suwOutline" style="padding-bottom:3px;">';
		body += '<div class="tbl full">';
		
		for(var a in this.points)
		{
			let _adCss = this.points[a].bold? 'font-weight:700;' : '' ;
			
			body += '<div class="td c b" style="font-size:0.8em; width:'+(100/this.points.length)+'%;'+_adCss+'">';
			body += '<span style="cursor:pointer;" data-suw-index="'+a+'">'+(this.points[a].label||this.points[a].val)+'</span></div>';
		}

		body += '</div>';

		let _sCss = 'height:'+this.suwHeight+'px;';

		if(this.options && this.options.addBg)
			_sCss += 'background:url(assets/img/suw_bg.png) center center repeat-y; background-size:cover;';

		body += '<div class="suwLayer" style="cursor:pointer; margin-top:5px; position:relative; border:1px solid #000; overflow:show;'+_sCss+'">';
		
		for(var a in this.points)
		{
			let _pCss = 'top:'+((this.suwHeight-this.pointSize)/2-1)+'px; width:'+this.pointSize+'px; height:'+this.pointSize+'px; border-radius:'+this.pointSize+'px;';

			if(this.points[a].bold)
				_pCss += '';

			body += '<div style="position:absolute; cursor:move; background-color:#fe582b; border:1px solid #666; box-shadow:0 0 2px #fff;'+_pCss+'">&nbsp;</div>';	
		}

		body += '</div>';
		body += '</div>';

		this.idDv.html(body);
	}

	private _uniqid(a:string='', b:boolean = false):string
	{
		var c = Date.now() / 1000;
		var d = c.toString(16).split(".").join("");
		while(d.length < 14){
			d += "0";
		}
		var e = "";
		if(b)
		{
			e = ".";
			e += Math.round(Math.random()*100000000);
		}
		return (a||'') + d + e;
	}
}