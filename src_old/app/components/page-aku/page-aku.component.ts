import { Component, OnInit } from '@angular/core';

import { appService } from "../../_services/app.service";
import { calcService } from "../../_services/calc.service";
import { suw } from "../../_models/suw";

@Component({
	selector: 'app-page-aku',
	templateUrl: './page-aku.component.html',
	styleUrls: ['./page-aku.component.css']
})
export class PageAkuComponent implements OnInit {

	suw_1:any;
	suw_11:any;
	suw_12:any;
	suw_13:any;
	suw_21:any;

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	constructor(
		private app: appService,
		private calc: calcService
	) { }

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	clickBtnRaport()
	{
		this.app.alert('Brak podpiętej funkcjonalności', 'Brak wytycznych co do wyglądu raportu akustycznego');
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	ngOnInit(): void {

		var self = this;

		let subscription = this.calc.observer$.subscribe((ts:string = '') => {
			if(ts == 'setPredefinedModel')
			{
				this.suw_1.setVal(this.calc.setData.sciana_zewn_rw);
				this.suw_11.setVal(this.calc.setData.okna_powierzchnia);
				this.suw_12.setVal(this.calc.setData.okna_szyby_pakiet);
				this.suw_13.setVal(this.calc.setData.okna_rw);
				this.suw_21.setVal(this.calc.setData.otoczenie);
			}
		});		

		var _fn = function(val:any, name:any=''){

			//console.log(x, y)
			self.calc.setData[name] = val;

			self.calc.pageChangeData('aku');

			if(name == 'okna_powierzchnia')
				self.calc.pageChangeData('wiz');
		}

		this.suw_1 = new suw('suw_sciana', this.calc.suwValues.sciana_zewn_rw, {val : this.calc.setData.sciana_zewn_rw}, _fn, 'sciana_zewn_rw');

		this.suw_11 = new suw('suw_powierzchnia', this.calc.suwValues.okna_powierzchnia, {val : this.calc.setData.okna_powierzchnia}, _fn, 'okna_powierzchnia');	
		this.suw_12 = new suw('suw_szyby', this.calc.suwValues.okna_szyby_pakiet, {val : this.calc.setData.okna_szyby_pakiet}, _fn, 'okna_szyby_pakiet');	
		this.suw_13 = new suw('suw_rw', this.calc.suwValues.okna_rw, {val : this.calc.setData.okna_rw}, _fn, 'okna_rw');
		
		this.suw_21 = new suw('suw_otoczenie', this.calc.suwValues.otoczenie, {val : this.calc.setData.otoczenie}, _fn, 'otoczenie');	
	}
}
