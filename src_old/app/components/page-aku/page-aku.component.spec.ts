import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageAkuComponent } from './page-aku.component';

describe('PageAkuComponent', () => {
  let component: PageAkuComponent;
  let fixture: ComponentFixture<PageAkuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageAkuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageAkuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
