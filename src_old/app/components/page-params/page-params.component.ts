import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../../app.component';

import { calcService } from "../../_services/calc.service";
import { appService } from "../../_services/app.service";


@Component({
	selector: 'app-page-params',
	templateUrl: './page-params.component.html',	
	styleUrls: ['./page-params.component.css']
})


export class PageParamsComponent implements OnInit {

	params:any = {
		findAddressStr : '',		
		rotate : 0,
		persons : 0,
	}

	constructor(
			private app: appService,
			private calc: calcService
		){
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	clickArrow(n:number)
	{
		let e:any = document.getElementById('houseRotator');
			
		if(n == 1)
			this.params.rotate = this.params.rotate + 45;
		else if(n == -1)
			this.params.rotate = this.params.rotate - 45;

		if(this.params.rotate == 360)
			this.params.rotate = 0;
		else if(this.params.rotate == -45)
			this.params.rotate = 315;

		if(n != 0)
			this.calc.setData.angle = this.params.rotate;

		this.calc.pageChangeData('paramsAngle');

		e.style.transform = 'rotate('+(-this.params.rotate)+'deg)';
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	onChangeInputData(t:any, v:any)
	{
		if(t == 'people_count')
		{
			var n = parseInt(v.value);

			if(!isNaN(n))
				this.calc.setData.persons = n;
		}
		else if(t == 'address')
		{

		}
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	clickSearch()
	{
		let i:any = document.getElementById('addressInput');
		this.calc.updateAddressFromInput(i.value);
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	clickNextButton()
	{
		if(this.calc.addressData.findAddress.length > 0 && this.calc.setData.persons > 0)
			this.calc.nextFormParams();
		else
			this.app.alert('Prosimy o podanie lokalizacji i ilości osób w budynku.');
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	ngOnInit(): void {
		this.params.rotate = this.calc.setData.angle;
		this.params.persons = this.calc.setData.persons;
		this.params.findAddressStr = this.calc.addressData.findAddress;
		//console.log(this.calc.addressData.findAddress)
		this.clickArrow(0);
	}
}
