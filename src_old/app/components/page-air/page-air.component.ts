import { Component, OnInit } from '@angular/core';

import { appService } from "../../_services/app.service";
import { calcService } from "../../_services/calc.service";
import { suw } from "../../_models/suw";

@Component({
	selector: 'app-page-air',
	templateUrl: './page-air.component.html',
	styleUrls: ['./page-air.component.css']
})
export class PageAirComponent implements OnInit {

	suw_1:any;
	suw_2:any;

	constructor(
		private app: appService,
		private calc: calcService
	)
	{}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	clickBtn(n:string)
	{
		this.app.alert('Brak podpiętej funkcjonalności', 'Brak wytycznych co do wyglądu dokumentu ('+n+')');
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	ngOnInit(): void {

		var self = this;
		
		let subscription = this.calc.observer$.subscribe((ts:string = '') => {
			if(ts == 'setPredefinedModel')
			{
				this.suw_1.setVal(this.calc.setData.budynek_szczelnosc);
				this.suw_2.setVal(this.calc.setData.wentylacja);
			}
		});

		var _fn = function(val:any, n:string)
		{
			self.calc.setData[n] = val;
			self.calc.pageChangeData('air');
		}		

		this.suw_1 = new suw('suw_szczelosc_bud', this.calc.suwValues.budynek_szczelnosc, {val : this.calc.setData.budynek_szczelnosc}, _fn, 'budynek_szczelnosc');
		this.suw_2 = new suw('suw_wentylacja', this.calc.suwValues.wentylacja, {val : this.calc.setData.wentylacja}, _fn, 'wentylacja');

	}

}
