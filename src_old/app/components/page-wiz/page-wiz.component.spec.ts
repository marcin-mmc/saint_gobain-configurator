import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageWizComponent } from './page-wiz.component';

describe('PageWizComponent', () => {
  let component: PageWizComponent;
  let fixture: ComponentFixture<PageWizComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageWizComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageWizComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
