import { Component, OnInit } from '@angular/core';

import { appService } from "../../_services/app.service";
import { calcService } from "../../_services/calc.service";
import { suw } from "../../_models/suw";

@Component({
	selector: 'app-page-wiz',
	templateUrl: './page-wiz.component.html',
	styleUrls: ['./page-wiz.component.css']
})
export class PageWizComponent implements OnInit {

	suw_1:any;
	suw_2:any;
	suw_3:any;

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	constructor(
		private app: appService,
		private calc: calcService
	)
	{}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	clickBtnRaport()
	{
		this.app.alert('Brak podpiętej funkcjonalności', 'Brak wytycznych co do wyglądu raportu części wizualnej');
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	ngOnInit(): void {

		var self = this;

		let subscription = this.calc.observer$.subscribe((ts:string = '') => {
			if(ts == 'setPredefinedModel')
			{
				this.suw_1.setVal(this.calc.setData.okna_powierzchnia);
				this.suw_2.setVal(this.calc.setData.okna_szyby_pakiet);
				this.suw_3.setVal(this.calc.setData.okna_zacienienie);
			}			
		});

		var _fn = function(val:any, idName:string){
			self.calc.setData.okna_powierzchnia = self.suw_1.currentValue;
			self.calc.setData.okna_szyby_pakiet = self.suw_2.currentValue;
			self.calc.setData.okna_zacienienie = self.suw_3.currentValue;
			self.calc.pageChangeData('wiz');
		}		

		this.suw_1 = new suw('suw_powierzchnia', this.calc.suwValues.okna_powierzchnia, {val : this.calc.setData.okna_powierzchnia}, _fn);
		this.suw_2 = new suw('suw_szyby', this.calc.suwValues.okna_szyby_pakiet, {val : this.calc.setData.okna_szyby_pakiet}, _fn);
		this.suw_3 = new suw('suw_zacienienie', this.calc.suwValues.okna_zacienienie, {val : this.calc.setData.okna_zacienienie}, _fn);		
	}

}


