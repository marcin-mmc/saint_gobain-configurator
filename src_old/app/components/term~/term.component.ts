import { Component, OnInit } from '@angular/core';

import { calcService } from "../../_services/calc.service";
import { suw } from "../../_models/suw";

//declare const $:any;
//declare const page: any;

@Component({
	selector: 'app-term',
	templateUrl: './term.component.html',
	styleUrls: ['./term.component.css']
})
export class TermComponent implements OnInit {

	suw_1:any;
	suw_2:any;
	suw_3:any;
	suw_4:any;
	suw_5:any;
	suw_11:any;
	suw_12:any;
	suw_13:any;
	suw_21:any;
	suw_22:any;
	suw_23:any;

	constructor(
		private calc: calcService
	){




	}

	clickBtnWycena():void
	{
		console.log('clickBtnWycena');
	}

	ngOnInit(): void {

		let subscription = this.calc.shapes$.subscribe((data) => {
			//this.storage = data;
			//console.log('term comp subscr', data);
			this.suw_1.setVal(this.calc.termData.wsp_u_sciana_zewn);
			this.suw_2.setVal(this.calc.termData.wsp_u_dach);
			this.suw_3.setVal(this.calc.termData.wsp_u_podloga);
			this.suw_4.setVal(this.calc.termData.wsp_u_drzwi);
			this.suw_5.setVal(this.calc.termData.wsp_u_okna);

			this.suw_11.setVal(this.calc.termData.okna_szczelnosc);
			this.suw_12.setVal(this.calc.termData.okna_wiatr);
			this.suw_13.setVal(this.calc.termData.okna_wentylacja);

			this.suw_21.setVal(this.calc.termData.energia_co);
			this.suw_22.setVal(this.calc.termData.energia_cwu);
			this.suw_23.setVal(this.calc.termData.energia_foto);

		});

		var points1 = [{val:0.35}, {val:0.30}, {val:0.25}, {val:0.20, bold:true}, {val:0.15, bold:true}, {val:0.10, bold:true}, {val:0.05}];
		var points2 = [{val:0.35}, {val:0.30}, {val:0.25}, {val:0.20}, {val:0.15, bold:true}, {val:0.13, bold:true}, {val:0.10, bold:true}, {val:0.05}];
		var points3 = [{val:0.35}, {val:0.30, bold:true}, {val:0.23, bold:true}, {val:0.20}, {val:0.15, bold:true}, {val:0.10}, {val:0.05}];
		var points4 = [{val:0.35}, {val:0.30}, {val:0.25}, {val:0.20}, {val:0.15}, {val:0.13, bold:true}, {val:0.09, bold:true}, {val:0.05}];
		var points5 = [{val:0.35}, {val:0.30}, {val:0.25}, {val:0.20}, {val:0.15}, {val:0.11, bold:true}, {val:0.10, bold:true}, {val:0.09, bold:true}, {val:0.05}];

		this.suw_1 = new suw('suw_wsp_u_zewn', points1, {addBg:true, val : this.calc.termData.wsp_u_sciana_zewn});
		this.suw_2 = new suw('suw_wsp_u_dach', points2, {addBg:true, val : this.calc.termData.wsp_u_dach});
		this.suw_3 = new suw('suw_wsp_u_podloga', points3, {addBg:true, val : this.calc.termData.wsp_u_podloga});
		this.suw_4 = new suw('suw_wsp_u_drzwi', points4, {addBg:true, val : this.calc.termData.wsp_u_drzwi});
		this.suw_5 = new suw('suw_wsp_u_okna', points5, {addBg:true, val : this.calc.termData.wsp_u_okna});

		this.suw_11 = new suw('suw_szczelosc_bud', [{val:3, bold:true}, {val:1.5, bold:true}, {val:0.6, bold:true}], {val:this.calc.termData.okna_szczelnosc});
		this.suw_12 = new suw('suw_oslona_wiatr', [{val:0, label:'brak', bold:true}, {val:1, label:'średnio', bold:true}, {val:2, label:'mocno', bold:true}], {val:this.calc.termData.okna_wiatr});
		this.suw_13 = new suw('suw_wentylacja', [{val:1, label:'grawitacyjna', bold:true}, {val:2, label:'mechaniczna<br>z odzyskiem', bold:true}, {val:3, label:'mechaniczna<br>z odzyskiem<br>/ czujnik CO<sub>2</sub>', bold:true}], {val:this.calc.termData.okna_wentylacja});

		this.suw_21 = new suw('suw_co', [{val:1, label:'en. elektryczna<br>bezpośrednia', bold:true}, {val:2, label:'gaz ziemny', bold:true}, {val:3, label:'pompa ciepła', bold:true}], {val:this.calc.termData.energia_co});
		this.suw_22 = new suw('suw_cwu', [{val:1, label:'en. elektryczna<br>bezpośrednia', bold:true}, {val:2, label:'gaz ziemny', bold:true}, {val:4, label:'pompa ciepła', bold:true}], {val:this.calc.termData.energia_cwu});
		this.suw_23 = new suw('suw_foto', [{val:1, label:'TAK', bold:true}, {val:0, label:'NIE', bold:true}], {val:this.calc.termData.energia_foto});

	}
}
