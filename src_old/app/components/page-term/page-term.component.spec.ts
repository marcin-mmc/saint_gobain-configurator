import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageTermComponent } from './page-term.component';

describe('PageTermComponent', () => {
  let component: PageTermComponent;
  let fixture: ComponentFixture<PageTermComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageTermComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageTermComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
