import { Component, OnInit } from '@angular/core';

import { calcService } from "../../_services/calc.service";

@Component({
	selector: 'app-rotator',
	templateUrl: './rotator.component.html',
	styleUrls: ['./rotator.component.css']
})

export class RotatorComponent implements OnInit {

	angle:number = 0;

	constructor(
		private calc : calcService
	) { }

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	clickArrow(n:number){
					
		if(n == 1)
			this.angle = this.angle + 45;
		else
			this.angle = this.angle - 45;
		
		this.rotateHouse(true);
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	rotateHouse(onArrow:boolean = false)
	{
		let e:any = document.getElementById('houseRotator');

		if(this.angle == 360)
			this.angle = 0;
		else if(this.angle == -45)
			this.angle = 315;

		//console.log('rotateHouse', this.angle);

		this.calc.setData.angle = this.angle;

		e.style.transform = 'rotate('+(-this.angle)+'deg)';		

		if(onArrow)
			this.calc.changeAngleFromRotator();
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	ngOnInit(): void {

		this.angle = this.calc.setData.angle;
		this.rotateHouse();
	}
}
