import { Injectable } from "@angular/core";
//import { Observable, throwError } from "rxjs";
import {
	HttpClient,
	HttpHeaders,
	HttpErrorResponse
} from "@angular/common/http";
//import { catchError, map } from "rxjs/operators";

// Set the http options
/*
const httpOptions = {
  //headers: new HttpHeaders({ "Content-Type": "application/json", "Authorization": "c31z" })
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};
*/

@Injectable({
	providedIn: 'root'
})


export class httpService {

	constructor(private http : HttpClient)
	{

	}

	public get(url:string, fn:any = false, param:any = false)
	{
		this.http.get<any>(url).subscribe(
				(res) => {

					if(typeof fn == 'function')
					{
						(param !== false)? fn.apply(null, [true, param, res]) : fn.apply(null, [true, res]) ;
					}
					
				},
				(err) => {
					if(typeof fn == 'function')
					{
						let errStr = err.statusText;
						(param !== false)? fn.apply(null, [false, param, errStr]) : fn.apply(null, [false, errStr]) ;
					}
				}
			);
	}

	public post(url:string, obj:any, fn:any = false, param:any = false)
	{
    	var corsHeaders = new HttpHeaders({
      		//'Content-Type': 'application/json',
      		//'Content-Type': 'text/plain'
      		//'Accept': 'json',
      		//'Accept': 'text/plain'
      		//'responseType' : 'text'
      		//'Access-Control-Allow-Origin': '*'
    	});

		var opt = {'headers': corsHeaders};
		let fd = new FormData();
		for(let a in obj)
		{
			if(typeof obj[a] == 'number' || typeof obj[a] == 'string')
				fd.append(a, obj[a]);
			else
				fd.append(a, JSON.stringify(obj[a]))
		}
		this.http.post<any>(url, fd, opt).subscribe(
		//this.http.post<any>(url, obj).subscribe(
				(res) => {

					if(typeof fn == 'function')
					{
						(param !== false)? fn.apply(null, [true, param, res]) : fn.apply(null, [true, res]) ;
					}
					
				},
				(err) => {
					if(typeof fn == 'function')
					{
						let errStr = err.statusText;
						(param !== false)? fn.apply(null, [false, param, errStr]) : fn.apply(null, [false, errStr]) ;
					}
				}
			);
	}

	public get1_yyyyyyyyyyy(url:string)
	{

		return this.http.get(url);


		//return this.http.get(url, httpOptions)


		//return "adasdas"//this.m_http.get(url)


	}
}