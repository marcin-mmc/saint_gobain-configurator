import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs'

import { httpService } from "../_services/http.service";
import { appService } from "../_services/app.service";
import { ImagesLoaderService } from "../_services/images-loader.service";


import * as predefinedModels_json from "../../assets/json/predefined_models.json";
import * as suwValues_json from "../../assets/json/suw_values.json";

import * as projekt1_json from "../../assets/json/projekt_01.json";

import { projekt_data_replace } from "../_models/projekt_data_replace";

@Injectable({
	providedIn: 'root'
})

export class calcService{

	private defaultData:any = (predefinedModels_json as any).default;	
	private projektData:any = (projekt1_json as any).default;
	private projektDataChanged:any;

	private meteoList:any;
	private meteoSelect:any;

	public isLoadedMap:boolean = false;
	public suwValues:any = (suwValues_json as any).default;
	public addressData:any = {input:'', findAddress:'', location : null};
	
	public setData:any = {
		persons:0,
		angle:0,
		findAddress:'',
		wsp_u_sciana_zewn : 0.35,
		wsp_u_dach : 0.35,
		wsp_u_podloga : 0.35,
		wsp_u_drzwi : 0.35,
		wsp_u_okna : 0.35,
		energia_co : 1,
		energia_cwu : 1,
		energia_foto : 0,
		budynek_szczelnosc : 3,
		oslona_wiatr : 0,
		wentylacja : 1,
		sciana_zewn_rw : 15,
		okna_powierzchnia	: 1,	
		okna_szyby_pakiet : 1,
		okna_zacienienie : 0,
		okna_rw : 32,
		otoczenie : 1
	};


	private predefinedData:any = {
		wsp_u_sciana_zewn : [0.2, 0.15, 0.1],
		wsp_u_dach : [0.15, 0.13, 1.1],
		wsp_u_podloga : [0.3, 0.23, 0.15],
		wsp_u_drzwi : [0.15, 0.13, 0.09],
		wsp_u_okna : [0.11, 0.1, 0.09],
		energia_co : [1, 2, 3],
		energia_cwu : [1, 2, 3],
		energia_foto : [0, 1, 1],
		budynek_szczelnosc : [3, 1.5, 0.6],
		oslona_wiatr : [0, 1, 2],
		wentylacja : [1, 2, 3],
		sciana_zewn_rw : [30, 35, 40],
		okna_powierzchnia	: [1,2,3],	
		okna_szyby_pakiet : [1,2,3],
		okna_zacienienie : [0, 1, 2],
		okna_rw : [32, 33, 36],
		otoczenie : [1, 2, 3]
	};

	private _shapesSource : Subject<any> = new Subject();
	observer$ = this._shapesSource.asObservable();

	constructor(
			private app : appService,
			private http : httpService,
			private imagesLoader : ImagesLoaderService
		){
		var self = this;

		setTimeout(function(){self.app.loader('Ładowanie danych bydynku');}, 100);
		setTimeout(function(){self.loadAllStartData();}, 1000);
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	private loadAllStartData()
	{
		var self = this;
		
		var data:any = {
			//calc_type : 'buildings_calc',
			calc_type : 'buildings_calc_report',
			data_json_str : this.projektData
		};

		var _fnMeteoLoadOk = function(ok:boolean, ret:any){
			self.app.loader('');
			//console.log(ret.meteo_data)
			self.meteoList = ret.meteo_data;
		};

		var _fnMeteoLoad = function(){
			self.app.loader('Ładowanie stacji meteo');			
			var dd = {data_json_str:'full_list', calc_type : 'meteo_data_list'};
			self.http.post('https://bimsources.com/api/bce/api.php?calc_country=pl', dd, _fnMeteoLoadOk);		
		};

		var _fnProjectLoadOk = function(ok:boolean, ret:any){			
			self.replaceData();			
			_fnMeteoLoad();
		};
		
		this.http.post('https://bimsources.com/api/bce/api.php?calc_country=pl', data, _fnProjectLoadOk);


		//this.imagesLoader.loadAll(function(){alert(1)});
	}

	private getDistance(p1:any, p2:any)
	{
		/*
		var R = 6378137; // Earth’s mean radius in meter
		var dLat = this._rad(p2.lat() - p1.lat());
		var dLong = this._rad(p2.lng() - p1.lng());
		var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(this._rad(p1.lat())) * Math.cos(this._rad(p2.lat())) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		var d = R * c;
		*/
		var R = 6378137; // Earth’s mean radius in meter
		var dLat = this._rad(p2[0] - p1[0]);
		var dLong = this._rad(p2[1] - p1[1]);
		var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(this._rad(p1[0])) * Math.cos(this._rad(p2[0])) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		var d = R * c;

		return d/1000; // returns the distance in meter
	}
	private _rad(x:number)
	{
		return x * Math.PI / 180;
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	private replaceData()
	{
		console.log(this.meteoSelect);
		console.log(this.projektData);

		var tmpProj = this.projektData;

		var t = new projekt_data_replace(this.projektData, this.setData);
		
		if(this.meteoSelect)
			t.setMeteId(this.meteoSelect.id);
		
		t.replace();

		this.projektDataChanged = tmpProj;
	}


	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	changeAngleFromRotator()
	{
		this.commitChanges('changeAngleFromRotator');
		this.replaceData();
	}

	setAddressDataFromGMaps(lc:any)
	{
		this.addressData.location = lc;

		//for(let i in this.meteoList)
		for(let i=0; i < this.meteoList.length; i++)
		{
			var dist = this.getDistance([this.meteoList[i].latitude, this.meteoList[i].longitude], [this.addressData.location.lat(), this.addressData.location.lng()]);
			//console.log(this.meteoList[i].desc, dist);
			this.meteoList[i]._distance = dist;
		}

		var my_dist = 100000;
		var _j = -1;

		for(let i=0; i < this.meteoList.length; i++)
		{
			if(this.meteoList[i]._distance < my_dist)
			{
				my_dist = this.meteoList[i]._distance;
				_j = i;
			}
		}	
		
		if(_j >= 0)
		{
			this.meteoSelect = this.meteoList[_j];
			this.app.alert('Na podstawie znalezionej lokalizacji wybrano stację meteo', this.meteoList[_j].desc);
			this.replaceData();
		}
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	commitChanges(typeStr:string=''){
		this._shapesSource.next(typeStr);
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	pageChangeData(n:string)
	{
		if(n == 'term')
			this.commitChanges('changeFromTerm');
		else if(n == 'air')
			this.commitChanges('changeFromAir');
		else if(n == 'aku')
			this.commitChanges('changeFromAku');
		else if(n == 'wiz')
			this.commitChanges('changeFromWiz');
		else if(n == 'paramsAngle')
		{
			this.commitChanges('changeFromParamsAngle');
			this.replaceData();
		}
		else if(n == 'term_windows')
			this.commitChanges('changeFromParamsWindows');	
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	updateAddressFromInput(s:string)
	{		
		if(s.length > 2)
		{
			this.addressData.input = s;
			this.commitChanges('changeAddressString');
		}
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	nextFormParams()
	{
		this.commitChanges('nextFormParams');
	}	

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	setPredefinedModel(n:number)
	{
		if(n >= 1 && n<= 3)
		{
			for(let a in this.predefinedData)
				this.setData[a] = this.predefinedData[a][n-1];			
			
			this.commitChanges('setPredefinedModel');
		}
	}
}
