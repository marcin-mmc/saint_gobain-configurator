import { Injectable } from '@angular/core';

import { httpService } from "../_services/http.service";
import { calcService } from "../_services/calc.service";

import * as const_values_json from "../../assets/json/const_values.json";
import * as config_json from "../../assets/config.json";

@Injectable({
	providedIn: 'root'
})
export class akuCalcService {

	private const_values:any = (const_values_json as any).default;
	private configData:any = (config_json as any).default;

	constructor(
		private http : httpService,
		private calc : calcService
	){

	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	get_rooms(ch:any)
	{
		//var JSON.parse(JSON.stringify(obj));
		var roomsData = this.calc.projektDataDesc.aku_params_rooms;
		var _wys = this.calc.projektDataDesc.aku_params.wysokosc;
		var _z = ["left", "top", "right", "bottom"];

		var _rooms:any = [];
		//var _walls:any = [];
		var _pow:number = 0;
		var _pow_win:number = 0;
		
		var _ra2r = this.calc.setData.sciana_zewn_rw;
		var _ra2r_o = this.calc.setData.okna_rw;
		var _dn_eA2R_w = this.calc.getConstValues('aku', 'vent_Ra2r');		
		var _went_rodzaj = this.calc.setData.wentylacja;
		
		var powierzchnia_okien = this.calc.setData.okna_powierzchnia; //w1 - duze, w2 - srednie, w3 - male

		for(let a in roomsData)
		{
			//_walls = [];
			_pow = 0;
			_pow_win = 0;

			for(let b in _z)
			{
				var _zn = _z[b];

				if(ch[_zn] && roomsData[a][_zn])
				{
					var rd = roomsData[a][_zn];
					//console.log(_zn, a, roomsData[a][_zn], rd.o)	
					//_walls.push({ S : rd.s, r_a2r : _ra2r, windows : [{ So : rd.o, r_a2r : _ra2r_o, quanity : rd.o?1:0 }], vents : [{ dn_eA2R : _dn_eA2R_w, quantity : 1 }]});
					
					_pow += rd.s || 0;
					_pow_win += rd.o || 0;
				}
			}
			
			if(_pow > 0)
			{
				var _pow_okna = _pow_win;

				if(powierzchnia_okien == 2)
					_pow_okna = _pow_okna * 0.873;
				else if(powierzchnia_okien == 3)
					_pow_okna = _pow_okna * 0.635;


				_rooms.push({V : _pow * _wys, walls : [{ S : _pow, r_a2r : _ra2r, windows : [{ So : _pow_okna, r_a2r : _ra2r_o, quanity : 1 }], vents : [{ dn_eA2R : _dn_eA2R_w, quantity : 1 }]}]});
			}
		}
		
		return _rooms;
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	calculate(ch:any, fn:any)
	{
		var _rooms = this.get_rooms(ch);
		//console.log(_rooms);
		var otoczenie = this.calc.setData.otoczenie;

		//var data:any = {terrain_type : 3, rooms:_rooms};
		var data:any = {terrain_type : otoczenie, rooms:_rooms};
		
		var dt:any = {calc_type : 'acoustic_calc', data_json_str : JSON.stringify(data)};
		
		//console.log(data)
		//console.log( JSON.stringify(data))

		var res:any = [];
		
		this.http.post(this.configData.api_url, dt, function(ok:boolean, ret:any){

			if(ok)
			{
				if(ret.results && ret.results.rooms)
				{
					console.log(ret.results.rooms);
					for(let a in ret.results.rooms)
					{
						res[a] = {
							day : ret.results.rooms[a].walls[0].day.stan >= 0,
							night : ret.results.rooms[a].walls[0].night.stan >= 0
						};
					}
					fn.apply(null, [res]);
				}
			}
			//console.log('results', ok, ret);
			//console.log(ret.results.rooms);
			//if(ok)
				//console.log(ret.results);
			//else
				//console.log('err', ret);
		});

		//return ret;
	}

}