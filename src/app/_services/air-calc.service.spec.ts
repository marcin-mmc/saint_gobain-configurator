import { TestBed } from '@angular/core/testing';

import { AirCalcService } from './air-calc.service';

describe('AirCalcService', () => {
  let service: AirCalcService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AirCalcService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
