import { TestBed } from '@angular/core/testing';

import { ImageLoaderService } from './images-loader.service';

describe('ImagesLoaderService', () => {
  let service: ImagesLoaderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ImageLoaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
