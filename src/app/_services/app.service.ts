import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs'

@Injectable({
	providedIn: 'root'
})

export class appService {

	constructor() { }

	private _shapesSource : Subject<any> = new Subject();
	observer$ = this._shapesSource.asObservable();

	commitChanges(typeStr:string=''){
		this._shapesSource.next(typeStr);
	}

	alert(s:string, addStr:string = '')
	{
		this._shapesSource.next({type:'alert', text:s, desc : addStr});
	}
	loader(s:string, addStr:string = '')
	{
		this._shapesSource.next({type:'loader', text:s, desc : addStr});
	}
	popup(s1:string, s2:string = '')
	{
		this._shapesSource.next({type:'popup', title:s1, body : s2});
	}	
}
