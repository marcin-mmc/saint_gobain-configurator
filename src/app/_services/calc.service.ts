import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs'

import { httpService } from "../_services/http.service";
import { appService } from "../_services/app.service";
import { ImagesLoaderService } from "../_services/images-loader.service";

import { projekt_data_replace } from "../_models/projekt_data_replace";

import * as projekt_json from "../../assets/json/projekt_02.json";
import * as projekt_desc_json from "../../assets/json/projekt_02_desc.json";
import * as suwValues_json from "../../assets/json/suw_values.json";


import * as elektr_co_json from "../../assets/json/co_cwu/elektr_co.json";
import * as elektr_cwu_json from "../../assets/json/co_cwu/elektr_cwu.json";
import * as gaz_co_json from "../../assets/json/co_cwu/gaz_co.json";
import * as gaz_cwu_json from "../../assets/json/co_cwu/gaz_co.json";
import * as pompa_co_json from "../../assets/json/co_cwu/pompa_co.json";
import * as pompa_cwu_json from "../../assets/json/co_cwu/pompa_co.json";
import * as const_values_json from "../../assets/json/const_values.json";

import * as config_json from "../../assets/config.json";

@Injectable({
	providedIn: 'root'
})

export class calcService{

	private projektData:any = (projekt_json as any).default;
	public projektDataDesc:any = (projekt_desc_json as any).default;
	private projektDataChanged:any;
	private const_values:any = (const_values_json as any).default;
	
	private configData:any = (config_json as any).default;

	private suwValuesJson:any = (suwValues_json as any).default;
	public suwValues:any;
	public setData:any = {
		persons:0,
		angle:0,
		findAddress:'',
		'...' : '...'
	};
	private predefinedData:any = {};	

	public resultsData:any = {};
	
	private meteoList:any;
	private meteoSelect:any;	

	public addressData:any = {input:'', findAddress:'', location : null};
	public isLoadedMap:boolean = false;	

	private dota_CO_CWU:any = {
		elektr_co : (elektr_co_json as any).default,
		elektr_cwu : (elektr_cwu_json as any).default,
		gaz_co : (gaz_co_json as any).default,
		gaz_cwu : (gaz_cwu_json as any).default,
		pompa_co : (pompa_co_json as any).default,
		pompa_cwu : (pompa_cwu_json as any).default
	}

	private _shapesSource : Subject<any> = new Subject();
	observer$ = this._shapesSource.asObservable();

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	constructor(
			private app : appService,
			private http : httpService,
			private imagesLoader : ImagesLoaderService
		){
		var self = this;

		this.suwValues = this.suwValuesJson.values;
		this.setData = this.suwValuesJson.start;
		this.predefinedData = this.suwValuesJson.defined;

		setTimeout(function(){self.app.loader('Ładowanie danych bydynku');}, 100);
		setTimeout(function(){self.loadAllStartData();}, 1000);
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	getConstValues(s:string, n:any=false)
	{
		if(n)
			return this.const_values[s][n];
		else
			return this.const_values[s];
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	private loadAllStartData()
	{
		var self = this;
		
		var data:any = {
			//calc_type : 'buildings_calc',
			calc_type : 'buildings_calc_report',
			data_json_str : this.projektData
		};

		var _fnMeteoLoadOk = function(ok:boolean, ret:any){
			self.app.loader('');
			//console.log(ret.meteo_data)
			self.meteoList = ret.meteo_data;
		};

		var _fnMeteoLoad = function(){
			self.app.loader('Ładowanie stacji meteo');			
			var dd = {data_json_str:'full_list', calc_type : 'meteo_data_list'};
			self.http.post(self.configData.api_url, dd, _fnMeteoLoadOk);		
		};

		var _fnProjectLoadOk = function(ok:boolean, ret:any){	
			self.calcData();
			_fnMeteoLoad();
		};
		
		this.http.post(this.configData.api_url, data, _fnProjectLoadOk);


		//this.imagesLoader.loadAll(function(){alert(1)});
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	private getDistance(p1:any, p2:any)
	{
		/*
		var R = 6378137; // Earth’s mean radius in meter
		var dLat = this._rad(p2.lat() - p1.lat());
		var dLong = this._rad(p2.lng() - p1.lng());
		var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(this._rad(p1.lat())) * Math.cos(this._rad(p2.lat())) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		var d = R * c;
		*/
		var R = 6378137; // Earth’s mean radius in meter
		var dLat = this._rad(p2[0] - p1[0]);
		var dLong = this._rad(p2[1] - p1[1]);
		var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(this._rad(p1[0])) * Math.cos(this._rad(p2[0])) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		var d = R * c;

		return d/1000; // returns the distance in meter
	}
	
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	private _rad(x:number)
	{
		return x * Math.PI / 180;
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	getProjectData()
	{
		var self = this;
		var t = new projekt_data_replace(this.projektData, this.setData);

		t.set_co_cwu(this.dota_CO_CWU);

		if(this.meteoSelect)
			t.setMeteId(this.meteoSelect.id);
		
		return t.replace();
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	private calcData()
	{
		var self = this;
		
		this.projektDataChanged = this.getProjectData();

		var sendDdata:any = {
			calc_type : 'buildings_calc_report',
			data_json_str : this.projektDataChanged
		};

		var _fnSendOk = function(ok:boolean, ret:any){			
			if(ok)
			{
				self.prepareDataFromResults(ret);
			}
			else
				self.app.alert('Błąd kalkulacji danych', ret);
		};
		
		this.resultsData = {};
		this.commitChanges('resultsChange');
		this.http.post(this.configData.api_url, sendDdata, _fnSendOk);
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	private prepareDataFromResults(res:any)
	{
		//console.log('calc.service', 'results', res.ek_table);

		this.resultsData = {};

		for(let a in res.ek_table)
		{
			if(res.ek_table[a].w_symbol == 'values')
			{
				//console.log(res.ek_table[a].h);
				this.resultsData.ek = this.zaok(res.ek_table[a].h, 2)+'';
				this.resultsData.ec = this.zaok(res.ek_table[a].c, 2)+'';
			}
		}

		this.resultsData.ep = this.zaok(res.results.results_building_designed.ep, 2)+'';

		this.resultsData.price_heat = this.zaok(res.results.results_building_designed.energy_costs_h, 2)+'';
		this.resultsData.price_air_cond = '???';
		
		this.commitChanges('resultsChange');
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	private commitChanges(typeStr:string=''){
		this._shapesSource.next(typeStr);
	}	

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	changeAngleFromRotator()
	{
		this.commitChanges('changeAngleFromRotator');
		this.calcData();
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	page(t:string, n:string)
	{
		this.commitChanges(t);
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	setAddressDataFromGMaps(lc:any, isOnStart:boolean = false)
	{
		this.addressData.location = lc;

		//for(let i in this.meteoList)
		for(let i=0; i < this.meteoList.length; i++)
		{
			var dist = this.getDistance([this.meteoList[i].latitude, this.meteoList[i].longitude], [this.addressData.location.lat(), this.addressData.location.lng()]);
			//console.log(this.meteoList[i].desc, dist);
			this.meteoList[i]._distance = dist;
		}

		var my_dist = 100000;
		var _j = -1;

		for(let i=0; i < this.meteoList.length; i++)
		{
			if(this.meteoList[i]._distance < my_dist)
			{
				my_dist = this.meteoList[i]._distance;
				_j = i;
			}
		}	
		
		if(_j >= 0)
		{
			this.meteoSelect = this.meteoList[_j];
			
			if(!isOnStart)
			{
				this.app.alert('Na podstawie znalezionej lokalizacji wybrano stację meteo', this.meteoList[_j].desc);
				this.calcData();
			}
		}
	}


	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	pageChangeData(n:string)
	{
		if(n == 'term')
			this.commitChanges('changeFromTerm');
		else if(n == 'air')
			this.commitChanges('changeFromAir');
		else if(n == 'aku')
			this.commitChanges('changeFromAku');
		else if(n == 'wiz')
			this.commitChanges('changeFromWiz');
		else if(n == 'paramsAngle')
			this.commitChanges('changeFromParamsAngle');			
		else if(n == 'term_windows')
			this.commitChanges('changeFromParamsWindows');	
		else if(n == 'persons_count')
			{}

		this.calcData();	
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	updateAddressFromInput(s:string)
	{		
		if(s.length > 2)
		{
			this.addressData.input = s;
			this.commitChanges('changeAddressString');
		}
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	nextFormParams()
	{
		this.commitChanges('nextFormParams');
	}	

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	setPredefinedModel(n:number)
	{
		if(n >= 1 && n<= 3)
		{
			for(let a in this.predefinedData)
				this.setData[a] = this.predefinedData[a][n-1];			
			
			this.commitChanges('setPredefinedModel');
			this.calcData();
		}
	}

	private zaok(num:number, decimals:any = null)
	{
		decimals = decimals == undefined? 2 : decimals;
		return Math.round(num*Math.pow(10, decimals))/Math.pow(10, decimals);
	}
}
