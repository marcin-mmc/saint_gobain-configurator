import { Injectable } from '@angular/core';

import { calcService } from "../_services/calc.service";
//import * as const_values_json from "../../assets/json/const_values.json";

@Injectable({
	providedIn: 'root'
})

export class AirCalcService {

	public ilosc_osob:number = 0;
	public kubatura_wentylowana:number;
	public powierzchnia_ogrzewana:number;
	public szczelnosc:number;
	public temperatura_zewnetrzna : number;

	private szczelnosc_test:boolean;	
	private strumien_wentylacyjny : number;	
	private temperatura_wewnetrzna : number;
	private wilgotnosc_zewn : number;	
	private bieg_rekuperatora:number;

	private co2_1_os : number;
	private co2_1_os_m3 : number;
	private zewn_co2_stezenie:number;

	public type:number = 1;

	//private const_values:any = (const_values_json as any).default;

	constructor(
			private calc : calcService
		){

		var airConst = this.calc.getConstValues('air');
		
		this.szczelnosc_test = airConst.szczelnosc_test;
		this.strumien_wentylacyjny = airConst.strumien_wentylacyjny;
		this.temperatura_zewnetrzna = airConst.temperatura_zewnetrzna;
		this.temperatura_wewnetrzna = airConst.temperatura_wewnetrzna
		this.wilgotnosc_zewn = airConst.wilgotnosc_zewn;
		this.bieg_rekuperatora = airConst.bieg_rekuperatora;
		this.co2_1_os = airConst.co2_1_os;
		this.co2_1_os_m3 = airConst.co2_1_os_m3;
		this.zewn_co2_stezenie = airConst.zewn_co2_stezenie;		
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	get constValues()
	{
		return {
			szczelnosc_test : this.szczelnosc_test,
			strumien_wentylacyjny : this.strumien_wentylacyjny,
			temperatura_zewnetrzna : this.temperatura_zewnetrzna,
			temperatura_wewnetrzna : this.temperatura_wewnetrzna,
			wilgotnosc_zewn : this.wilgotnosc_zewn,
			bieg_rekuperatora : this.bieg_rekuperatora,
			co2_1_os : this.co2_1_os,
			co2_1_os_m3 : this.co2_1_os_m3,
			zewn_co2_stezenie : this.zewn_co2_stezenie,
		};
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	przelicz()
	{
		var ilosc_wilgoci:number = 0;
		var ilosc_wilgoci_m3:number = 0;
		var wilgotnosc_wzgledna:number = 0;
		var stezenie_co2:number = 0;
		
		if(this.type == 1)
		{
			var Vinf = this.szczelnosc_test? 0.05 * this.szczelnosc * this.kubatura_wentylowana : 0.2 * this.kubatura_wentylowana;	
			var Vo = 0.31 * this.powierzchnia_ogrzewana ;
			var Vn = Vinf + Vo;
			
			var kr_wymiany = Vn / this.kubatura_wentylowana;
			
			ilosc_wilgoci = this.ilosc_wilgoci();
			ilosc_wilgoci_m3 =ilosc_wilgoci / Vn;


			var ilosc_wilg_zewn = 216.7 * (((this.wilgotnosc_zewn / 100)* 6.112 * Math.exp((17.62 * this.temperatura_zewnetrzna)/(243.12+this.temperatura_zewnetrzna)))/(273.15+this.temperatura_zewnetrzna));

			var suma_wilgoci = ilosc_wilgoci_m3 + ilosc_wilg_zewn;

			//=(100*A51*(273,15+A24))/(216,7*6,112*EXP((17,62*A24)/(243,12+A24)))
			wilgotnosc_wzgledna = (100 * suma_wilgoci * (273.15 + this.temperatura_zewnetrzna)) / (216.7 * 6.112 * Math.exp((17.62 * this.temperatura_wewnetrzna) / (243.12 + this.temperatura_wewnetrzna))) ;

			var stezenie_co2 = Math.pow(10, 6) * (this.ilosc_osob / this.kubatura_wentylowana) * (this.co2_1_os_m3 / kr_wymiany) + this.zewn_co2_stezenie;

			var il_str_went_parter = kr_wymiany * this.kubatura_wentylowana;
			var il_str_went_os = il_str_went_parter / this.ilosc_osob;
		}
		else if(this.type == 2 || this.type == 3)
		{
			var Vinf = this.szczelnosc_test? 0.05 * this.szczelnosc * this.kubatura_wentylowana : 0.2 * this.kubatura_wentylowana;
			var Vo = 0;

			if(this.bieg_rekuperatora == 1)
				Vo = 0.3 * this.strumien_wentylacyjny;
			else if(this.bieg_rekuperatora == 2)
				Vo = 0.5 * this.strumien_wentylacyjny;
			else if(this.bieg_rekuperatora == 3)
				Vo = 0.9 * this.strumien_wentylacyjny;

			var Vn = Vinf + Vo;
			var kr_wymiany = Vn / this.kubatura_wentylowana;

			ilosc_wilgoci = this.ilosc_wilgoci();
			ilosc_wilgoci_m3 =ilosc_wilgoci / Vn;
			//console.log(ilosc_wilgoci);

			//=216,7*(((A23/100)*6,112*EXP((17,62*A22)/(243,12+A22)))/(273,15+A22))
			var ilosc_wilg_zewn = 216.7 * (((this.wilgotnosc_zewn / 100)* 6.112 * Math.exp((17.62 * this.temperatura_zewnetrzna)/(243.12+this.temperatura_zewnetrzna)))/(273.15+this.temperatura_zewnetrzna));

			var suma_wilgoci = ilosc_wilgoci_m3 + ilosc_wilg_zewn;

			//=(100*A51*(273,15+A24))/(216,7*6,112*EXP((17,62*A24)/(243,12+A24)))
			wilgotnosc_wzgledna = (100 * suma_wilgoci * (273.15 + this.temperatura_zewnetrzna)) / (216.7 * 6.112 * Math.exp((17.62 * this.temperatura_wewnetrzna) / (243.12 + this.temperatura_wewnetrzna))) ;

			//=10^6*(A60/A61)*(A65/B39)+A62
			var stezenie_co2 = Math.pow(10, 6) * (this.ilosc_osob / this.kubatura_wentylowana) * (this.co2_1_os_m3 / kr_wymiany) + this.zewn_co2_stezenie;
			

			var il_str_went_parter = kr_wymiany * this.kubatura_wentylowana;
			var il_str_went_os = il_str_went_parter / this.ilosc_osob;

			if(this.type == 3)
			{
				ilosc_wilgoci *= 0.5;
				ilosc_wilgoci_m3 *= 0.5;
				wilgotnosc_wzgledna *= 0.5;
				stezenie_co2 *= 0.5;
			}
		}

		return {
			ilosc_wilgoci : this.zaok(ilosc_wilgoci, 2),
			ilosc_wilgoci_m3 : this.zaok(ilosc_wilgoci_m3, 3),
			wilgotnosc_proc : this.zaok(wilgotnosc_wzgledna, 1),
			stezenie_co2 : this.zaok(stezenie_co2, 2)
		}		
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	
	private ilosc_wilgoci()
	{
		var rd = 12 * 5 / 24 / 7;
		var lz = this.ilosc_osob * 2 * 1100 / 24 / 7;		
		var ps = this.ilosc_osob * 2 * 5 * 1600 / 24 / 7;
		var kp = 14 * 70 / 24 / 7;
		var dp = 7 * 200 / 24 / 7;
		var zm = 7 * 200 / 24 / 7;
		var pr = 4 * 300 / 24 / 7;
		var sc = 28 * 50 / 24 / 7;
		var ac = 28 * 80 / 24 / 7;
		var sp = 4 * 3200 / 24 / 7;

		
		return rd + lz + ps + kp + dp + zm + pr + sc + ac + sp;
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	private zaok(num:number, decimals:any = null)
	{
		decimals = decimals == undefined? 2 : decimals;
		return Math.round(num*Math.pow(10, decimals))/Math.pow(10, decimals);
	}	
}
