import { Injectable } from '@angular/core';

import * as rotatedImages_json from "../../assets/json/rotated_images.json";

@Injectable({
	providedIn: 'root'
})
export class ImagesLoaderService {

	private rotatedImages2Load:any = (rotatedImages_json as any).default;
	
	countLoad:number = 0;
	callableFn:any;
	
	constructor() { }

	public loadAll(fn:any)
	{		
		alert('Nie ładuje jak trzeba');
		this.callableFn = fn;

		this.loadCurrent();
	}

	loadCurrent()
	{
		var self = this;
		var _fn = function(){self.loadOneOk();}		
		var im = this.rotatedImages2Load[this.countLoad];
		
		this.loadOneImage('assets/img/rotate_house/' + im, _fn);
	}

	loadOneOk()
	{
		this.countLoad ++;
		
		if(this.countLoad < this.rotatedImages2Load.length)
			this.loadCurrent();
		else
			this.callableFn.apply();
	}

	loadOneImage(src:string, fn:any)
	{
		var im = new Image();
		
		im.onload = function(){
			fn.apply();
		};
		im.src = src;		
	}		
}
