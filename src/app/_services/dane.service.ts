import { Injectable } from '@angular/core';

import { httpService } from "../_services/http.service";
//import { AppStartComponent } from "../app-start/app-start.component";

//import * as stropy_json from "../../assets/json/stropy.json";

@Injectable({
	providedIn: 'root'
})

export class daneService {

	//private stropy_lista:any = (stropy_json as any).default;
	//private sciany_lista:any = [];

	private callbackFn:any;

	constructor(
		private http: httpService,
	) {

		//const url = 'https://firma.hybrid.pl/marcin/';
		//const url = 'https://test.hybrid.pl/hph/bim/api/?co=multitool&what=multitool_data_2';
		//const url = 'http://localhost/aku.json';
		//const url = '../assets/aku-new.json';
		
		//var self = this;
		//this.http.get(url, function(a:boolean, b:any){self.returnLoadUrl(a, b)});

		//this.loadAllData();
	}

	loadAllData(data_url:string)
	{
		var self = this;

		//console.log('data_url', data_url);
		
		//const url = 'https://test.hybrid.pl/hph/bim/api/?co=multitool&what=multitool_data_2';		
		this.http.get(data_url, function(a:boolean, b:any){self.returnLoadUrl(a, b)});
	}

	setFunctionLoadOk(fn:any)
	{
		this.callbackFn = fn;
	}

	returnLoadUrl	(ok:boolean, m:any)
	{
		//console.log('returnLoadUrl', ok, m)
		var ret : any = [];
		if(ok)
		{
			this.callbackFn.apply(null, [true]);
		}
		else
			this.callbackFn.apply(null, [false, m]);
	}
}
