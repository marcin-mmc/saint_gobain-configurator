import { TestBed } from '@angular/core/testing';

import { akuCalcService } from './aku-calc.service';

describe('akuCalcService', () => {
  let service: akuCalcService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(akuCalcService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
