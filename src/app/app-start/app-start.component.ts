import { Component, OnInit, AfterViewInit, ElementRef, ViewChild } from '@angular/core';

import { daneService } from "../_services/dane.service";
import { httpService } from "../_services/http.service";

import * as config_json from "../../assets/config.json";

@Component({
	selector: 'app-app-start',
	templateUrl: './app-start.component.html',
	styleUrls: ['./app-start.component.css']
})

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

export class AppStartComponent implements OnInit {

	//@ViewChild('ramka_iframe') iframe : ElementRef;

	//private config_data:any = (config_json as any).default;
	raport_php : string = '';	
	dane_url  : string = '';

	constructor(
		private dane: daneService,
		private http: httpService
	){
		var self = this;
		this.dane.setFunctionLoadOk(function(a:boolean, b:any){self.loadData(a, b)});		
	}
	
	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	
	loadData(is:boolean, e:any='')
	{
		if(is)
		{

		}
		else
			alert("Aplikacja akustyczna\nBłąd ładowania danych" + "\n\n" +e);
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	ngOnInit(): void
	{
	
	}


	ngAfterViewInit(): void
	{
		/*
		let x: HTMLIFrameElement = document.getElementById('ramka_iframe') as HTMLIFrameElement;
		this.iframe.nativeElement.contentWindow.addEventListener("message", function(e:any){
			//console.log('akustyka odbiera wiadomość', e);
			}, false);
		*/
		//this.dane.loadAllData(this.dane_url);

		//setTimeout(function(){self.pmsg.send({application : 'acoustic', action : 'applicationLoad', value : true});}, 100);
	}
}
