import { Component } from '@angular/core';

import { appService } from "./_services/app.service";
import { calcService } from "./_services/calc.service";

declare const $:any;

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})


export class AppComponent {

	currentPage:string = 'aku';//params

	//currentPage:string = 'term';

	constructor(
		private app: appService,
		private calc: calcService
	){ }

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	clickMenu(s:string)
	{
		this.currentPage = s;
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	clickPredefinedModel(n:number):void
	{
		//console.log('clickPredefinedModel', n);
		this.calc.setPredefinedModel(n);
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -	
	
	changeSizeApp()
	{
		var dd:any = $('#leftElem');
		//console.log('dd.style.height', dd.height());
		$('#currentPage').css('min-height', dd.height());
		//$('#pageDv').css('background', 'red');
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -	

	ngAfterViewInit(): void
	{
		var self = this;
		this.changeSizeApp();
		
		$(window).resize(function(){
			self.changeSizeApp();
		});
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -	

	ngOnInit(): void
	{
		//this.app.loader('askdjhakdjhadkda skdjh kjfhkj');
		this.changeSizeApp();

		this.calc.observer$.subscribe((ts:any = '', add:any = null) => {		

			if(ts == 'setPredefinedModel')
				{}
			else if(ts == 'changeAngleFromRotator' || ts == 'changeFromWiz')
				{}
			else if(ts == 'changeFromAku')
				{}
			else if(ts == 'nextFormParams')
			{
				this.currentPage = 'term';
			}
			else if(ts == 'changeFromParamsAngle' || ts == 'changeFromParamsWindows')
				{}
			else if(ts == 'resultsChange')
			{
				
			}
			else
				{}//console.log('app.component.observer', ts);

			this.changeSizeApp();	
		});		
	}
}