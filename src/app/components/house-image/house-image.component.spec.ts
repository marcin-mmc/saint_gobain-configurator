import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HouseImageComponent } from './house-image.component';

describe('HouseImageComponent', () => {
  let component: HouseImageComponent;
  let fixture: ComponentFixture<HouseImageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HouseImageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HouseImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
