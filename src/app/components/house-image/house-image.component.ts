import { Component, OnInit } from '@angular/core';

import { calcService } from "../../_services/calc.service";

declare const $:any;

@Component({
	selector: 'app-house-image',
	templateUrl: './house-image.component.html',
	styleUrls: ['./house-image.component.css']
})
export class HouseImageComponent implements OnInit {

	imageRotateName:string = '_default.jpg';

	constructor(
		private calc: calcService
	){}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -	

	private updateImage()
	{
		var self = this;
		
		var fn2 = function(){$('.image').animate({opacity:1});};

		var fn1 = function(){
			self.changeImageName();
			setTimeout(fn2, 500);
		};		
		
		$('.image').animate({opacity:0.1}, 100, fn1);			
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -	

	private changeImageName()
	{
		var self = this;
		
		//rotate_house
		//100_okna__widok_1.jpg
	/*
	powierzchnia
	szyby
	zacienienie
	*/	
		var pw = '100';
		var zc = '';
		var kt = 1;
		
		if(this.calc.setData.okna_powierzchnia == 2)
			pw = '70';
		else if(this.calc.setData.okna_powierzchnia == 3)
			pw = '40';

		if(this.calc.setData.okna_zacienienie == 1)
			zc = '_rolety';
		else if(this.calc.setData.okna_zacienienie == 2)
			zc = '_okap';

		var d:any = {0:6, 45:5, 90:4, 135:3, 180:2, 225:1, 270:8, 315:7};
		
		if(d[this.calc.setData.angle])
			kt = d[this.calc.setData.angle];

		var src = pw + '_okna'+zc+'__widok_'+kt+'.jpg';
		
		this.imageRotateName = src;
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -	

	ngOnInit(): void {

		var self = this;
		
		this.calc.observer$.subscribe((ts:any = '', add:any = null) => {

			if(ts == 'setPredefinedModel')
				self.updateImage();
			else if(ts == 'changeAngleFromRotator' || ts == 'changeFromWiz')
				self.updateImage();
			else if(ts == 'changeFromAku')
				self.updateImage();
			else if(ts == 'changeFromParamsAngle' || ts == 'changeFromParamsWindows')
				self.updateImage();
			
		});

		this.updateImage();
	}
}
