import { Component, OnInit } from '@angular/core';
import { appService } from "../../_services/app.service";

@Component({
	selector: 'app-data-sheet',
	templateUrl: './data-sheet.component.html',
	styleUrls: ['./data-sheet.component.css']
})
export class DataSheetComponent implements OnInit {

	constructor(
		private app:appService
	)
	{}

	clickBtn()
	{
		this.app.alert('Brak podpiętej funkcjonalności', 'Brak wytycznych co do wyglądu raportu części wizualnej');
	}

	ngOnInit(): void {
	}

}
