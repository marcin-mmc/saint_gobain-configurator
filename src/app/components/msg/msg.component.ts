import { Component, OnInit } from '@angular/core';

import { appService } from "../../_services/app.service";

declare const $:any;

@Component({
	selector: 'app-msg',
	templateUrl: './msg.component.html',
	styleUrls: ['./msg.component.css']
})

export class MsgComponent implements OnInit {

	//_bg:any = '#bg';
	//_alert:any;

	titleTxt:string = '';
	msgTxt:string = '';
	descriptionTxt:string = '';
	type:string = '';

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	
	constructor(
		private app:appService
	) {
		
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	private alert(s:string, addStr:any = null)
	{
		this.type = 'alert';
		this.msgTxt = s;
		this.descriptionTxt = addStr;

		this.showBg();
		
		$('#alert').show().css('opacity', 0);

		setTimeout(function(){
			var atWidth = $('#alert').outerWidth(true);
			var atHeight = $('#alert').outerHeight(true);			
			
			$('#alert').css({left : 'calc(50% - '+(atWidth/2)+'px)', top : 'calc(50% - '+(atHeight/2)+'px)'});
			$('#alert').animate({opacity:1});
		}, 100);		
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	private popup(s1:string, s2:string)
	{
		var self = this;
		
		this.type = 'popup';		
		this.titleTxt = s1;
		this.msgTxt = s2;

		this.showBg();
		
		$('#popup').show().css('opacity', 0);

		setTimeout(function(){
			var atWidth = $('#popup').outerWidth(true);
			var atHeight = $('#popup').outerHeight(true);			
			
			$('#popup').css({left : 'calc(50% - '+(atWidth/2)+'px)', top : 'calc(50% - '+(atHeight/2)+'px)'});
			$('#popup').animate({opacity:1});
			$('#close').show();
			self.setElementsPos();
		}, 100);
		this.setElementsPos();
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	private loader(s:any)
	{
		var self = this;

		if(s)
		{
			if(this.type == '')
			{
				this.showBg();
				$('#loader').show().css('opacity', 0);
			}
			
			this.msgTxt = s;				

			setTimeout(function(){
				var atWidth = $('#loader').outerWidth(true);
				var atHeight = $('#loader').outerHeight(true);			
				
				$('#loader').css({left : 'calc(50% - '+(atWidth/2)+'px)', top : 'calc(50% - '+(atHeight/2)+'px)'});
				
				if(self.type == '')
					$('#loader').animate({opacity:1});

				self.type = 'loader';
			
			}, 100);

			//this.type = 'loader';
		}
		else
		{
			$('#loader').fadeOut();
			$('#bg').fadeOut();
			this.type = '';
		}
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	private showBg()
	{
		$('#bg').fadeIn();
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	okClick()
	{
		$('#alert').fadeOut();
		$('#bg').fadeOut();
		this.type = '';
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	closeClick()
	{
		$('#popup').fadeOut();
		$('#bg').fadeOut();
		$('#close').hide();
		this.type = '';		
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	private setElementsPos()
	{
		if(this.type == 'popup')
		{
			var x = $('#popup').outerWidth(true) + $('#popup').position().left;
			var y = $('#popup').position().top;
			$('#close').css({left:x, top:y});
		}
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	private getSvgImageStr(type:string):string
	{
		var str = '';

		if(type == 'loader')
		{
			str = '<svg width="57" height="57" viewBox="0 0 57 57" xmlns="http://www.w3.org/2000/svg" stroke="#aaa">';
			str += '<g fill="none" fill-rule="evenodd">';
			str += '<g transform="translate(1 1)" stroke-width="2">';
			str += '<circle cx="5" cy="50" r="5">';
			str += '<animate attributeName="cy" begin="0s" dur="2.2s" values="50;5;50;50" calcMode="linear" repeatCount="indefinite" />';
			str += '<animate attributeName="cx" begin="0s" dur="2.2s" values="5;27;49;5" calcMode="linear" repeatCount="indefinite" />';
			str += '</circle>';
			str += '<circle cx="27" cy="5" r="5">';
			str += '<animate attributeName="cy" begin="0s" dur="2.2s" from="5" to="5" values="5;50;50;5" calcMode="linear" repeatCount="indefinite" />';
			str += '<animate attributeName="cx" begin="0s" dur="2.2s" from="27" to="27" values="27;49;5;27" calcMode="linear" repeatCount="indefinite" />';
			str += '</circle>';
			str += '<circle cx="49" cy="50" r="5">';
			str += '<animate attributeName="cy" begin="0s" dur="2.2s" values="50;50;5;50" calcMode="linear" repeatCount="indefinite" />';
			str += '<animate attributeName="cx" from="49" to="49" begin="0s" dur="2.2s" values="49;5;27;49" calcMode="linear" repeatCount="indefinite" />';
			str += '</circle>';
			str += '</g>';
			str += '</g>';
			str += '</svg>';
		}
		return str;
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	
	ngOnInit(): void {

		var self = this;

		//this._bg = document.getElementById('bg');
		//this._alert = document.getElementById('alert');
		
		this.app.observer$.subscribe((ts:any = '') => {
			if(typeof ts == 'object' && ts.type)
			{
				if(ts.type == 'alert')
				{
					if(ts.desc)
						self.alert(ts.text, ts.desc);
					else
						self.alert(ts.text, ts.desc);
				}
				else if(ts.type == 'loader')
				{
					self.loader(ts.text);
				}
				else if(ts.type == 'popup')
					self.popup(ts.title, ts.body);
			}			
		});

		$(window).resize(function(){
			self.setElementsPos();
		});

		this.setElementsPos();

		$('#close').click(function(){self.closeClick();});
	}
}