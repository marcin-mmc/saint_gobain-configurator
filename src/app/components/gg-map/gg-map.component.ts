import { Component, OnInit, Input} from '@angular/core';

import { calcService } from "../../_services/calc.service";
import { appService } from "../../_services/app.service";

declare var google:any;

@Component({
	selector: 'app-gg-map',
	templateUrl: './gg-map.component.html',
	//template : '<script src="http://maps.google.com/maps/api/js?key=AIzaSyBPPtXhJUmeGudFiId0jDPMSRmITszApjk" type="text/javascript"></script>',
	styleUrls: ['./gg-map.component.css']
})


export class GgMapComponent implements OnInit {

	@Input() heightMap: any = 100;

	htmlId:string = 'g-map';
	ggMap:any;	
	
	constructor(
		private app : appService,
		private calc : calcService
	){}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	ngOnInit(): void {
		
		var self = this;
		
		if(!this.calc.isLoadedMap)
			this.loadGoogleScript();
		else
			this.scriptLoadOk();

		let subscription = this.calc.observer$.subscribe((ts:any = '') => {
			if(ts == 'changeAddressString')
				self.searchByAddress(this.calc.addressData.input);
		});
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	alert(s:string)
	{
		this.app.alert('Lokalizacja', s);
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	private searchByAddress(addressStrig:string)
	{
		var self = this;
		var geokoder = new google.maps.Geocoder();

		var obslugaGeokodowania = function(wyniki:any, status:any)
		{
			//console.log('obslugaGeokodowania', status, wyniki);
			if(status == google.maps.GeocoderStatus.OK)
			{
				var a = wyniki[0].geometry.location;
				//callFunction.apply(null, [true, a.lat(), a.lng(), wyniki[0].formatted_address]);

				self.calc.addressData.findAddress = wyniki[0].formatted_address;

				
				self.setMarker(a);
			}
			else
			{
				self.alert('Nie znaleziono wyszukiwanego adresu: '+addressStrig);
			}
		}
		
		geokoder.geocode( { address: addressStrig } , obslugaGeokodowania);
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	setMarker(locationObj:any, is_start:boolean = false)
	{
		this.calc.setAddressDataFromGMaps(locationObj, is_start);

		var marker = new google.maps.Marker({	map: this.ggMap, position: locationObj });
								
		this.ggMap.panTo(marker.position);
		this.ggMap.setZoom(22);
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	private scriptLoadOk()
	{
		var self = this;

		var wsp = new google.maps.LatLng(52.17393169256846, 19.324951171875); //polska

		var map_option = {
		  zoom: 5,
		  center: wsp,
		  mapTypeId: google.maps.MapTypeId.ROADMAP
		};
						
		this.ggMap = new google.maps.Map(document.getElementById(this.htmlId), map_option);

		if(this.calc.addressData.location)
		{
			setTimeout(function(){
				self.setMarker(self.calc.addressData.location, true);	
			}, 1000);			
		}
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	private loadGoogleScript()
	{
		var self = this;

		let scriptElement = document.createElement("script");
		scriptElement.type = "text/javascript";
		scriptElement.async = true;

		scriptElement.src = 'https://maps.google.com/maps/api/js?key=AIzaSyBPPtXhJUmeGudFiId0jDPMSRmITszApjk';
		//scriptElement.src = 'http://localhost/aa.js';

		scriptElement.onload = () => {
			self.scriptLoadOk();
		};

		//document.getElementsByTagName('body')[0].appendChild(scriptElement);
		let d:any = document.getElementById(this.htmlId);//
		d.appendChild(scriptElement);
		this.calc.isLoadedMap = true;
	}
}
