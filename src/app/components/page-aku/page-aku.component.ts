import { Component, OnInit } from '@angular/core';

import { appService } from "../../_services/app.service";
import { calcService } from "../../_services/calc.service";
import { akuCalcService } from "../../_services/aku-calc.service";

import { suw } from "../../_models/suw";

declare const $:any;

@Component({
	selector: 'app-page-aku',
	templateUrl: './page-aku.component.html',
	styleUrls: ['./page-aku.component.css']
})

export class PageAkuComponent implements OnInit {

	suw_1:any;
	suw_11:any;
	suw_12:any;
	suw_13:any;
	suw_21:any;

	checked:any = {top:false, right:false, bottom:false, left:false};

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	constructor(
		private app: appService,
		private calc: calcService,
		private aku_calc : akuCalcService
	) { }

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	clickBtnRaport()
	{
		this.app.alert('Brak podpiętej funkcjonalności', 'Brak wytycznych co do wyglądu raportu akustycznego');
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	changeCheckbox(s:string, em:any)
	{
		this.checked[s] = em.checked;
		this.calculate();
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	calculate()
	{
		var fn = function(a:any){
			console.log('calculate-ret', a);
		};


		this.aku_calc.calculate(this.checked, fn);		
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	setSize()
	{
		var w:number = $('#resContainer').width();
		$('#resContainer').css('height', ((710/585) * w) + 'px');
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	ngAfterViewInit()
	{
		var self = this;

		var _fn = function(val:any, name:any=''){

			//console.log(x, y)
			self.calc.setData[name] = val;

			self.calc.pageChangeData('aku');

			if(name == 'okna_powierzchnia')
				self.calc.pageChangeData('wiz');

			self.calculate();
		}

		this.suw_1 = new suw('suw_sciana', this.calc.suwValues.sciana_zewn_rw, {val : this.calc.setData.sciana_zewn_rw}, _fn, 'sciana_zewn_rw');

		this.suw_11 = new suw('suw_powierzchnia', this.calc.suwValues.okna_powierzchnia, {val : this.calc.setData.okna_powierzchnia}, _fn, 'okna_powierzchnia');	
		this.suw_12 = new suw('suw_szyby', this.calc.suwValues.okna_szyby_pakiet, {val : this.calc.setData.okna_szyby_pakiet}, _fn, 'okna_szyby_pakiet');	
		this.suw_13 = new suw('suw_rw', this.calc.suwValues.okna_rw, {val : this.calc.setData.okna_rw}, _fn, 'okna_rw');
		
		this.suw_21 = new suw('suw_otoczenie', this.calc.suwValues.otoczenie, {val : this.calc.setData.otoczenie}, _fn, 'otoczenie');			

		this.calc.page('loadPage', 'aku');
		
		this.setSize();
		setTimeout(function(){self.setSize();}, 100);

		this.calculate();
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	ngOnInit(): void {

		var self = this;

		let subscription = this.calc.observer$.subscribe((ts:string = '') => {
			if(ts == 'setPredefinedModel')
			{
				this.suw_1.setVal(this.calc.setData.sciana_zewn_rw);
				this.suw_11.setVal(this.calc.setData.okna_powierzchnia);
				this.suw_12.setVal(this.calc.setData.okna_szyby_pakiet);
				this.suw_13.setVal(this.calc.setData.okna_rw);
				this.suw_21.setVal(this.calc.setData.otoczenie);
				//this.calculate();
			}
			else if(ts == 'resultsChange')
			{
				//this.calculate();
			}
		});


		this.calc.page('startPage', 'aku');		
		
		$(window).resize(function(){self.setSize();});
	}
}
