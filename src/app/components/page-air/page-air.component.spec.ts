import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageAirComponent } from './page-air.component';

describe('PageAirComponent', () => {
  let component: PageAirComponent;
  let fixture: ComponentFixture<PageAirComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageAirComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageAirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
