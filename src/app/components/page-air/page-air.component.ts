import { Component, OnInit } from '@angular/core';

import { appService } from "../../_services/app.service";
import { calcService } from "../../_services/calc.service";
import { AirCalcService } from "../../_services/air-calc.service";

import { suw } from "../../_models/suw";
import { size } from "../../_models/size";

declare const $:any;

@Component({
	selector: 'app-page-air',
	templateUrl: './page-air.component.html',
	styleUrls: ['./page-air.component.css']
})
export class PageAirComponent implements OnInit {

	suw_1:any;
	suw_2:any;
	suw_3:any;

	comment:any;

	resultsData : any = {
		ilosc_wilgoci : '',
		ilosc_wilgoci_m3 : '',
		stezenie_co2 : ''	
	};

	constValues:any = {};

	constructor(
		private app: appService,
		private calc: calcService,
		private air_calc : AirCalcService
	)
	{
		this.constValues = this.air_calc.constValues;
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	clickBtn(n:string)
	{
		if(n == 'zalozenia')
		{
			//nthis.constValues = this.air_calc.constValues;
			this.app.popup('Założenia wyliczeń', this.getDivText('zalozenia_wyliczen'));
		}	
		else
			this.app.alert('Brak podpiętej funkcjonalności', 'Brak wytycznych co do wyglądu dokumentu ('+n+')');
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	clickImage(v:number)
	{
		this.calc.setData['wentylacja'] = v;
		this.calc.pageChangeData('air');
		this.change3dImages();
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	calculate()
	{
		this.air_calc.type = this.calc.setData.wentylacja;
		this.air_calc.ilosc_osob = this.calc.setData.persons;
		this.air_calc.kubatura_wentylowana = 200;//this.alc.projektData.???
		this.air_calc.powierzchnia_ogrzewana = 150;//this.alc.projektData.???
		this.air_calc.szczelnosc = this.calc.setData.budynek_szczelnosc;
		this.air_calc.temperatura_zewnetrzna = this.calc.setData.temp_zewn_pory;

		var ret = this.air_calc.przelicz();

		this.resultsData = {
			ilosc_wilgoci : ret.ilosc_wilgoci,
			ilosc_wilgoci_m3 : ret.ilosc_wilgoci_m3,
			stezenie_co2 : ret.stezenie_co2,
			wilgotnosc_wzgledna : ret.wilgotnosc_proc
		};
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	private getDivText(id:string)
	{
		var e:any = document.getElementById(id);
		return e.innerHTML;
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	change3dImages()
	{
		var act = this.calc.setData.wentylacja;
		
		for(let i=1; i<=3; i++)
		{			
			if(i == act)
				$('#images3D .tr.item'+i).removeClass('set').addClass('set');
			else
				$('#images3D .tr.item'+i).removeClass('set');
		}
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	ngAfterContentInit ()
	{
		var self = this;
		this.calculate();

		setTimeout(function(){

			//self.clickBtn('zalozenia');

		}, 2000);		
	}	

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	ngAfterViewInit()
	{
		var self = this;

		var _fn = function(val:any, n:string)
		{
			self.calc.setData[n] = val;
			self.calc.pageChangeData('air');
			self.change3dImages();
		}

		this.suw_1 = new suw('suw_szczelosc_bud', this.calc.suwValues.budynek_szczelnosc, {val : this.calc.setData.budynek_szczelnosc}, _fn, 'budynek_szczelnosc');
		this.suw_2 = new suw('suw_wentylacja', this.calc.suwValues.wentylacja, {val : this.calc.setData.wentylacja}, _fn, 'wentylacja');
		this.suw_3 = new suw('suw_temp_pory', this.calc.suwValues.temp_zewn_pory, {val : this.calc.setData.temp_zewn_pory}, _fn, 'temp_zewn_pory');	

		this.calc.page('loadPage', 'air');

		this.calculate();

		this.change3dImages();
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	ngOnInit(): void {

		var self = this;
		
		let subscription = this.calc.observer$.subscribe((ts:string = '') => {
			if(ts == 'setPredefinedModel')
			{
				this.suw_1.setVal(this.calc.setData.budynek_szczelnosc);
				this.suw_2.setVal(this.calc.setData.wentylacja);
				this.calculate();
			}
			else if(ts == 'resultsChange')
				this.calculate();

			self.change3dImages();
		});

		this.calc.page('startPage', 'air');

		var s = new size('#rightSideContent');
		s.setHeightAsHeight('#rightSide');		
	}
}
