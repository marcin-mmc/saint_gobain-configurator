import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../../app.component';

import { calcService } from "../../_services/calc.service";
import { appService } from "../../_services/app.service";

import { size } from "../../_models/size";

declare const $:any;

@Component({
	selector: 'app-page-params',
	templateUrl: './page-params.component.html',	
	styleUrls: ['./page-params.component.css']
})


export class PageParamsComponent implements OnInit {

	elementMapWidth:any = 100;

	params:any = {
		findAddressStr : '',		
		rotate : 0,
	}

	descData:any = {
		/*
		buildingName : 'EcoReadyHouse - H1',
		buildingLoc : 'Tychowo /k.Stargardu',
		buildingType : 'niskoenergetyczny, jednorodzinny',
		buildingTechn : 'prefabrykat i szkielet drewniany',
		buildingPow : '143',
		buildingYear : '2014',
		buildingWyk : 'EcoReadyHouse'
		*/
	};

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	constructor(
			private app: appService,
			private calc: calcService
		){		
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	clickArrow(n:number)
	{
		let e:any = document.getElementById('houseRotator');
			
		if(n == 1)
			this.params.rotate = this.params.rotate + 45;
		else if(n == -1)
			this.params.rotate = this.params.rotate - 45;

		if(this.params.rotate == 360)
			this.params.rotate = 0;
		else if(this.params.rotate == -45)
			this.params.rotate = 315;

		if(n != 0)
		{
			this.calc.setData.angle = this.params.rotate;
			this.calc.pageChangeData('paramsAngle');			
		}
		

		if(e)
			e.style.transform = 'rotate('+(-this.params.rotate)+'deg)';	
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	onChangeInputData(t:any, v:any)
	{
		if(t == 'address')
		{

		}
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	clickSearch()
	{
		let i:any = document.getElementById('addressInput');
		this.calc.updateAddressFromInput(i.value);
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	clickNextButton()
	{
		if(this.calc.addressData.findAddress.length > 0 && this.calc.setData.persons > 0)
			this.calc.nextFormParams();
		else
			this.app.alert('Prosimy o podanie lokalizacji i ilości osób w budynku.');
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	setSize()
	{
		var w = $('#mapHouse').width();
		var nw:any = w > 500? 500 : nw;
		
		$('#mapHouse').css('height', nw);
		$('#mapHouse > div').css('height', nw);
		this.elementMapWidth = nw;
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	ngAfterViewInit()
	{
		var self = this;
		this.calc.page('loadPage', 'params');
		
		$(window).resize(function(){
			self.setSize();
		});
		
		//self.setSize();
		setTimeout(function(){self.setSize();}, 100);
		setTimeout(function(){self.setSize();}, 1000);
		this.clickArrow(0);
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	ngOnInit(): void {

		this.params.rotate = this.calc.setData.angle;
		this.params.findAddressStr = this.calc.addressData.findAddress;
		//console.log(this.calc.addressData.findAddress)
		//this.clickArrow(0);
		
		this.descData = this.calc.projektDataDesc;

		this.calc.page('startPage', 'params');

		var s = new size('#rightSideContent');
		s.setHeightAsHeight('#rightSide')
	}
}
