import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonsCountComponent } from './persons-count.component';

describe('PersonsCountComponent', () => {
  let component: PersonsCountComponent;
  let fixture: ComponentFixture<PersonsCountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonsCountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonsCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
