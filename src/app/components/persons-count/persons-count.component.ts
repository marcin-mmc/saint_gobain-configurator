import { Component, OnInit } from '@angular/core';

import { calcService } from "../../_services/calc.service";
import { appService } from "../../_services/app.service";

declare const $:any;

@Component({
	selector: 'app-persons-count',
	templateUrl: './persons-count.component.html',
	styleUrls: ['./persons-count.component.css']
})



export class PersonsCountComponent implements OnInit {

	constructor(
		private app: appService,
		private calc: calcService
	){}

	count_persons:number = 0;

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	onChangeInputData(v:any)
	{
		var n = parseInt(v.value);

		if(!isNaN(n))
		{
			this.calc.setData.persons = n;
			this.calc.pageChangeData('persons_count');
		}
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	ngOnInit(): void {
		this.count_persons = this.calc.setData.persons;
	}

}
