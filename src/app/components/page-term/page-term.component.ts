import { Component, OnInit } from '@angular/core';

import { calcService } from "../../_services/calc.service";
import { appService } from "../../_services/app.service";
import { suw } from "../../_models/suw";

import { size } from "../../_models/size";

declare const Chart: any;

@Component({
	selector: 'app-page-term',
	templateUrl: './page-term.component.html',
	styleUrls: ['./page-term.component.css']
})


export class PageTermComponent implements OnInit {

	suw_1:any;
	suw_2:any;
	suw_3:any;
	suw_4:any;
	suw_5:any;
	suw_11:any;
	suw_12:any;
	suw_13:any;
	suw_21:any;
	suw_22:any;
	suw_23:any;
	suw_31:any;
	suw_32:any;
	//suw_33:any;
	suw_34:any;

	resultsData:any;

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	constructor(
		private app: appService,
		private calc: calcService
	) { }

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	clickBtnRaport():void
	{
		this.app.alert('Brak podpiętej funkcjonalności', 'Brak wytycznych co do wyglądu raportu');
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	ngOnInit(): void {

		var self = this;

		let subscription = this.calc.observer$.subscribe((ts:string) => {
			//this.storage = data;
			//console.log('term comp subscr', s);
			if(ts == 'setPredefinedModel')
			{
				this.suw_1.setVal(this.calc.setData.wsp_u_sciana_zewn);
				this.suw_2.setVal(this.calc.setData.wsp_u_dach);
				this.suw_3.setVal(this.calc.setData.wsp_u_podloga);
				this.suw_4.setVal(this.calc.setData.wsp_u_drzwi);
				this.suw_5.setVal(this.calc.setData.wsp_u_okna);

				this.suw_11.setVal(this.calc.setData.okna_powierzchnia);
				this.suw_12.setVal(this.calc.setData.okna_szyby_pakiet);
				this.suw_13.setVal(this.calc.setData.okna_zacienienie);

				this.suw_21.setVal(this.calc.setData.budynek_szczelnosc);
				this.suw_22.setVal(this.calc.setData.oslona_wiatr);
				this.suw_23.setVal(this.calc.setData.wentylacja);

				this.suw_31.setVal(this.calc.setData.energia_co);
				this.suw_32.setVal(this.calc.setData.energia_cwu);
				//this.suw_33.setVal(this.calc.setData.energia_foto);
				this.suw_34.setVal(this.calc.setData.system_chlodzenia);
			}
			else if(ts == 'resultsChange')
				this.resultsData = this.calc.resultsData;
				//this.resultsData = {};
			//else if(ts == 'resultsNew')
				
		});

		this.resultsData = this.calc.resultsData;

		this.calc.page('startPage', 'term');

		var s = new size('#rightSideContent');
		s.setHeightAsHeight('#rightSide');
	}

	ngAfterViewInit()
	{
		var self = this;
		var _fn = function(val:any, n:string)
		{
			self.calc.setData[n] = val;

			if(n == 'okna_powierzchnia' || n == 'okna_szyby_pakiet' || n == 'okna_zacienienie')
			{
				self.calc.pageChangeData('term_windows');
			}
			else
				self.calc.pageChangeData('term');
		}
				
		this.suw_1 = new suw('suw_wsp_u_zewn', this.calc.suwValues.wsp_u_sciana_zewn, {addBg:true, val : this.calc.setData.wsp_u_sciana_zewn}, _fn, 'wsp_u_sciana_zewn');
		this.suw_2 = new suw('suw_wsp_u_dach', this.calc.suwValues.wsp_u_dach, {addBg:true, val : this.calc.setData.wsp_u_dach}, _fn, 'wsp_u_dach');
		this.suw_3 = new suw('suw_wsp_u_podloga', this.calc.suwValues.wsp_u_podloga, {addBg:true, val : this.calc.setData.wsp_u_podloga}, _fn, 'wsp_u_podloga');
		this.suw_4 = new suw('suw_wsp_u_drzwi', this.calc.suwValues.wsp_u_drzwi, {addBg:true, val : this.calc.setData.wsp_u_drzwi}, _fn, 'wsp_u_drzwi');
		this.suw_5 = new suw('suw_wsp_u_okna', this.calc.suwValues.wsp_u_okna, {addBg:true, val : this.calc.setData.wsp_u_okna}, _fn, 'wsp_u_okna');

		this.suw_11 = new suw('suw_okna_powierzchnia', this.calc.suwValues.okna_powierzchnia, {val : this.calc.setData.okna_powierzchnia}, _fn, 'okna_powierzchnia');
		this.suw_12 = new suw('suw_okna_szyby', this.calc.suwValues.okna_szyby_pakiet, {val : this.calc.setData.okna_szyby_pakiet}, _fn, 'okna_szyby_pakiet');
		this.suw_13 = new suw('suw_okna_zacienienie', this.calc.suwValues.okna_zacienienie, {val : this.calc.setData.okna_zacienienie}, _fn, 'okna_zacienienie');

		this.suw_21 = new suw('suw_szczelosc_bud', this.calc.suwValues.budynek_szczelnosc, {val: this.calc.setData.budynek_szczelnosc}, _fn, 'budynek_szczelnosc');
		this.suw_22 = new suw('suw_oslona_wiatr', this.calc.suwValues.oslona_wiatr, {val : this.calc.setData.oslona_wiatr}, _fn, 'oslona_wiatr');
		this.suw_23 = new suw('suw_wentylacja', this.calc.suwValues.wentylacja, {val : this.calc.setData.wentylacja}, _fn, 'wentylacja');

		this.suw_31 = new suw('suw_co', this.calc.suwValues.energia_co, {val : this.calc.setData.energia_co}, _fn, 'energia_co');
		this.suw_32 = new suw('suw_cwu', this.calc.suwValues.energia_cwu, {val : this.calc.setData.energia_cwu}, _fn, 'energia_cwu');
		//this.suw_33 = new suw('suw_foto', this.calc.suwValues.energia_foto, {val : this.calc.setData.energia_foto}, _fn, 'energia_foto');
		this.suw_34 = new suw('system_chlodzenia', this.calc.suwValues.system_chlodzenia, {val : this.calc.setData.system_chlodzenia}, _fn, 'system_chlodzenia');
		


		var ctx:any = document.getElementById("charttttt")
		var cty = ctx.getContext("2d");
		
		var c:any = {
			type: "doughnut",
			data : {
				labels: ['Podłogi', 'Dach', 'Ściany', 'Stolarka'],
				datasets: [
					{data:[20,30,10,40], backgroundColor:['#fcd9c1', '#fde9db', '#bef3b4', '#e3edf7']},
				]
			},
			options : {responsive: false, showAllTooltips: true}
		};

		var mc = new Chart(cty, c);		

		setTimeout(function(){mc.data.datasets[0].data = [10,40,20,30];mc.update();}, 5000);

		this.calc.page('loadPage', 'term');
	}

}
