//declare const $:any;

export class projekt_data_replace
{
	o:any;
	oSet:any;

	cc:any;

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	constructor(obj:any, objSet:any)
	{
		this.o = this._set_o(obj);
		this.oSet = this._set_o(objSet);
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	setMeteId(id:any)
	{
		this.o.meteo_id = id;
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	set_co_cwu(o:any)
	{
		this.cc = o;
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

/*	oSet
		persons:0,
		angle:0,
		findAddress:'',
		wsp_u_sciana_zewn : 0.35,
		wsp_u_dach : 0.35,
		wsp_u_podloga : 0.35,
		wsp_u_drzwi : 0.35,
		wsp_u_okna : 0.35,
		energia_co : 1,
		energia_cwu : 1,
		energia_foto : 0, //usunięta !!!!!!!!!!!
		system_chlodzenia : 0,
		budynek_szczelnosc : 3,
		oslona_wiatr : 0,
		wentylacja : 1,
		sciana_zewn_rw : 15,
		okna_powierzchnia	: 1,	
		okna_szyby_pakiet : 1,
		okna_zacienienie : 0,
		okna_rw : 32,
		otoczenie : 1
*/

	replace()
	{
		var Q = this.o;
		
		for(let i=0; i<Q.zones.length; i++)
		{
			//zmiana ilości osób
			Q.zones[i]['number_of_people'] = this.oSet.persons;

			if(Q.zones[i].envelope)
			{
				for(let j=0; j<Q.zones[i].envelope.length; j++)
				{
					//console.log(Q.zones[i].envelope[j].bim_type)
					if(Q.zones[i].envelope[j].bim_type)
					{
						var _bt = Q.zones[i].envelope[j].bim_type;
						var _loc = Q.zones[i].envelope[j].location;
						
						//zmiana orientacji
						if(_bt == 1 || _bt == 4)
						{
							Q.zones[i].envelope[j].geog = this._change_orent_by_angle(Q.zones[i].envelope[j].geog);

							//console.log('new-geog', Q.zones[i].envelope[j].geog, Q.zones[i].envelope[j].name);
						}
						
						//sciana zewnętrzna U
						if(_bt == 1 && _loc == 1)
						{
							Q.zones[i].envelope[j].item_U = this.oSet.wsp_u_sciana_zewn;
							//console.log('setU', Q.zones[i].envelope[j].item_U);
						}

						//Dach, stropodach						
						if((_bt == 3 || _bt == 4 || _bt == 9) && _loc == 1)
						{
							Q.zones[i].envelope[j].item_U = this.oSet.wsp_u_dach;
							//console.log('setU', Q.zones[i].envelope[j].item_U);
						}

						//Podłoga na gruncie
						if(_bt == 2 && _loc == 1)
						{
							Q.zones[i].envelope[j].item_U = this.oSet.wsp_u_podloga;
							//console.log('setU', Q.zones[i].envelope[j].item_U);
						}						

						//Drzwi i okna połaciowe
						if(Q.zones[i].envelope[j].windows && Q.zones[i].envelope[j].windows.length > 0)
						{
							for(let k=0; k<Q.zones[i].envelope[j].windows.length; k++)
							{
								if(Q.zones[i].envelope[j].windows[k].bim_type == 22)
								{
									Q.zones[i].envelope[j].windows[k].item_U = this.oSet.wsp_u_drzwi;
									//console.log('setU', Q.zones[i].envelope[j].windows[k].item_U);
								}
								else if(Q.zones[i].envelope[j].windows[k].bim_type == 21)
								{
									Q.zones[i].envelope[j].windows[k].item_U = this.oSet.wsp_u_okna;
									//console.log('setU', Q.zones[i].envelope[j].windows[k].item_U);
								}								
							}
						}

						//powierzchnie okien
						if(Q.zones[i].envelope[j].windows && Q.zones[i].envelope[j].windows.length > 0)
						{
							var _wsp_pow = 1;
							var _wsp_szb = [0.38, 0.9];
							var _wsp_zac = [1,1];
							
							if(this.oSet.okna_powierzchnia == 2)
								_wsp_pow = 0.873;
							else if(this.oSet.okna_powierzchnia == 3)
								_wsp_pow = 0.635;
							
							if(this.oSet.okna_szyby_pakiet == 2)
								_wsp_szb = [0.54, 0.9];
							else if(this.oSet.okna_szyby_pakiet == 3)
								_wsp_szb = [0.6, 0.8];


							if(this.oSet.okna_zacienienie == 1)
								_wsp_zac = [0.91, 1];
							else if(this.oSet.okna_zacienienie == 2)
								_wsp_zac = [1, 0.6];

							for(let k=0; k<Q.zones[i].envelope[j].windows.length; k++)
							{
								//Powierzchnia
								Q.zones[i].envelope[j].windows[k].size_a *= _wsp_pow;
								Q.zones[i].envelope[j].windows[k].size_b *= _wsp_pow;

								//Pakiet szybowy
								//Q.zones[i].envelope[j].windows[k].item_C *= _wsp_szb;
								Q.zones[i].envelope[j].windows[k].item_g = _wsp_szb[0];
								Q.zones[i].envelope[j].windows[k].item_U = _wsp_szb[1];

								//Zacienienie
								Q.zones[i].envelope[j].windows[k].sh_F_value = _wsp_zac[0];
								Q.zones[i].envelope[j].windows[k].sh_F_mov = _wsp_zac[1];

								Q.zones[i].envelope[j].windows[k].sh_F_data_status = 0;

								//console.log('okna', 'size_ab-'+i+''+j+''+k, Q.zones[i].envelope[j].windows[k].size_b);
							}
						}
					}

					//Szczelnosc budynku
					Q.zones[i].went_n_50_status = 1;
					//Q.zones[i].went_n_50 = this.oSet.budynek_szczelnosc;

					//Osłona przed wiatrem
					//Q.zones[i].vent_factor_e = this.oSet.oslona_wiatr;

					//Wentylacja
					if(this.oSet.wentylacja == 1)
					{
						Q.zones[i].ventilation_type = 1;
					}
					else if(this.oSet.wentylacja == 2)
					{
						Q.zones[i].ventilation_type = 5;
						Q.zones[i].eta_oc1 = 0.9;
					}
					else if(this.oSet.wentylacja == 3)
					{
						Q.zones[i].ventilation_type = 5;
						Q.zones[i].eta_oc1 = 0.9;
					}
				}	
			}
		}

		//Szczelnosc budynku
		Q.went_n_50 = this.oSet.budynek_szczelnosc;

		//Osłona przed wiatrem
		Q.vent_factor_e = this.oSet.oslona_wiatr;		

		//CO
		if(this.oSet.energia_co == 1)
		{			
			Q.heating_systems_prj = this.cc.elektr_co;
			Q.heating_systems_alt = this.cc.elektr_co;
		}
		else if(this.oSet.energia_co == 2)
		{			
			Q.heating_systems_prj = this.cc.gaz_co;
			Q.heating_systems_alt = this.cc.gaz_co;
		}
		else if(this.oSet.energia_co == 3)
		{			
			Q.heating_systems_prj = this.cc.pompa_co;
			Q.heating_systems_alt = this.cc.pompa_co;
		}

		//CWU
		if(this.oSet.energia_cwu == 1)
		{			
			Q.DHW_systems_prj   = this.cc.elektr_cwu;
			Q.DHW_systems_alt  = this.cc.elektr_cwu;
		}
		else if(this.oSet.energia_cwu == 2)
		{			
			Q.DHW_systems_prj = this.cc.gaz_cwu;
			Q.DHW_systems_alt = this.cc.gaz_cwu;
		}
		else if(this.oSet.energia_cwu == 3)
		{			
			Q.DHW_systems_prj = this.cc.pompa_cwu;
			Q.DHW_systems_alt = this.cc.pompa_cwu;
		}		

		return Q;
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	//południe - S
	//północ N
	//wschód E
	//zachód W

	//o 90 stopni z s na w

	private _change_orent_by_angle(geog:string)
	{
		var kt1 = ['S', 'SW', 'W', 'NW', 'N', 'NE', 'E', 'SE'];
		var kt2:any = {'S' : 0, 'SW' : 315, 'W' : 270, 'NW' : 225, 'N' : 180, 'NE' : 135, 'E' : 90, 'SE' : 45};

		if(kt1.indexOf(geog) != -1)
		{
			var _kat = kt2[geog];
			var _kat_n = (_kat + this.oSet.angle) % 360;

			return this._get_obj_key(kt2, _kat_n);
		}
				
		return null;
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	private _set_o(obj:any)
	{
		return JSON.parse(JSON.stringify(obj));
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	private _get_obj_key(obj:any, val:any, def:any = false)
	{
		for(let a in obj)
		{
			if(obj[a] == val)
				return a;
		}
		return def;
	}
}