declare const $:any;

export class size
{
	eName = '';
	_e = '';
	mn:number = 1;
	
	private type:string = '';
	private strict:boolean = false;

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	constructor(e:any)
	{
		this.eName = e;
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	setHeightAsWidth(e:any, m:number = 1, strict:boolean = false)
	{
		this.set('haw', e, m, strict);
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	setHeightAsHeight(e:any, m:number = 1, strict:boolean = false)
	{
		this.set('hah', e, m, strict);
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	private set(type:string, e:any, m:number = 1, strict:boolean = false)
	{
		var self = this;

		this.type = type;
		this.strict = strict;

		this._e = e;
		this.mn = m;

		this.setSize();

		$(window).resize(function(){
			self.setSize();
		});

		setTimeout(function(){self.setSize();}, 100);		
	}

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	private setSize()
	{
		if(this.type == 'hah')
		{
			var h = $(this._e).height();
			$(this.eName).css(this.strict? 'height' : 'min-height' , (h*this.mn)+'px');
		}
		else if(this.type == 'haw')
		{
			var w = $(this._e).width();			
			$(this.eName).css(this.strict? 'height' : 'min-height' , (w*this.mn)+'px');
		}		
	 }
}